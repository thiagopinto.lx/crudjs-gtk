const { GObject, Gtk, Gda, GLib } = imports.gi;
let connection = null;
let conf = null;

var Connection = GObject.registerClass({
    GTypeName: 'Conexao'
}, class Connection extends Gda.Connection {
      _init(config) {

        if (!connection) {
          if(config !== undefined){
            conf = config;
          }
          connection = super._init(conf);
          connection.open();
          let sqlQuery;
          try {
            sqlQuery = `SELECT * FROM contact`;
            var dm = connection.execute_select_command(sqlQuery);
            log(dm);
          } catch (e) {
            sqlQuery = `create table contact(
                              id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                              firstname VARCHAR(100),
                              lastname VARCHAR(100),
                              contact VARCHAR(100)
                            )`;
            connection.execute_non_select_command(sqlQuery);
          }
      }

        return connection;

      }
});
