const { GObject, Gtk, Gda, GLib } = imports.gi;
const { Connection } = imports.connection;

var Contact = GObject.registerClass({
    GTypeName: 'Contact'

}, class Contact extends GObject.Object {

      _init(config) {
        this._connection = new Connection();
        super._init(config);
        this._id = null;
        this._firstname = null;
        this._lastname = null;
        this._contact = null;
      }

      _create_where(build, field, value){

          let id_field = build.add_id(field);
          let id_value = build.add_expr_value(null, value);
          let id_cond = build.add_cond(Gda.SqlOperatorType.EQ, id_field, id_value, 0);

          return id_cond;
      }

      set_firstname(firstname){
        this._firstname = firstname;
      }

      get_firstname(firstname){
        return this._firstname;
      }

      set_lastname(lastname){
        this._lastname = lastname;
      }

      get_lastname(lastname){
        return this._lastname;
      }

      set_contact(contact){
        this._contact = contact;
      }

      get_contact(contact){
        return this._contact;
      }


      static get_all(){
        let connection = new Connection();
        let dm = connection.execute_select_command("select * from contact");
        let iter = dm.create_iter ();

        return iter;
        let text = "";

        while (iter.move_next ()) {
          let id_field = Gda.value_stringify (iter.get_value_at(0));
          let firstname_field = Gda.value_stringify (iter.get_value_at(1));

          text += id_field + "\t=>\t" + firstname_field + '\n';
        }

        log(text);

      }


      _create(){

        let item = new Gda.SqlBuilder({stmt_type:Gda.SqlStatementType.INSERT});

        if(this._firstname != null && this._contact != null){
          item.set_table ("contact");
          item.add_field_value_as_gvalue("firstname", this._firstname);
          if(this._lastname != null){
            item.add_field_value_as_gvalue("lastname", this._lastname);
          }
          item.add_field_value_as_gvalue("contact", this._contact);
          let stmt = item.get_statement();

          try {
            this._connection.statement_execute_non_select(stmt, null);
            let data = this._connection.execute_select_command("select  last_insert_rowid()");
            let id = Gda.value_stringify(data.get_value_at(0, 0));
            this._id = id;
            return true;
          }catch (err) {
            log(err);
            return false;
          }
        }

      }

      _update(){
        let item = new Gda.SqlBuilder({stmt_type:Gda.SqlStatementType.UPDATE});

        if(this._firstname != null && this._contact != null){

          item.set_where(this._create_where(item, "id", this._id));
          item.set_table ("contact");
          item.add_field_value_as_gvalue("firstname", this._firstname);
          if(this._lastname != null){
            item.add_field_value_as_gvalue("lastname", this._lastname);
          }
          item.add_field_value_as_gvalue("contact", this._contact);
          let stmt = item.get_statement();

          try {
            this._connection.statement_execute_non_select(stmt, null);

            return true;
          }catch (err) {
            log(err);
            return false;
          }
        }
      }

      save(){
        if (this._id != null) {
          this._update();
        }else{
          this._create();
        }
      }

      get(id){
        return true
      }



      delete(id){
        let item = new Gda.SqlBuilder({stmt_type:Gda.SqlStatementType.DELETE});

          item.set_where(this._create_where(item, "id", this._id));
          item.set_table ("contact");
          let stmt = item.get_statement();
          try {
            this._connection.statement_execute_non_select(stmt, null);
            return true;
          }catch (err) {
            log(err);
            return false;
          }

        return true;
      }
});
