/* main.js
 *
 * Copyright 2019 Thiago Pinto Dias
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

pkg.initGettext();
pkg.initFormat();
pkg.require({
  'Gio': '2.0',
  'Gtk': '3.0',
  'Gda': '5.0'
});

const { Gio, Gtk, Gda, GLib } = imports.gi;

const { CrudjsWindow } = imports.window;

const { Connection } = imports.connection;
const { Contact } = imports.contact;

function main(argv) {
    let config = { provider: Gda.Config.get_provider("SQLite"),
                   cnc_string: `DB_DIR=${GLib.get_user_data_dir()};DB_NAME=crudjs`
                  };
    const connection = new Connection(config);

    const application = new Gtk.Application({
        application_id: 'org.gnome.crudjs',
        flags: Gio.ApplicationFlags.FLAGS_NONE,
    });

    application.connect('activate', app => {
        let activeWindow = app.activeWindow;
        
        if (!activeWindow) {
            activeWindow = new CrudjsWindow(app);
        }

        activeWindow.present();
    });
    log(GLib.get_user_data_dir());
    //let contact = new Contact();
    //contact._id = 6;
    //contact.set_firstname("Pedro");
    //contact.set_lastname("Pinto");
    //contact.set_contact("0000-0007");

    //contact.save();
    //Contact.get_all();
    return application.run(argv);
}
