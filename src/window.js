/* window.js
 *
 * Copyright 2019 Thiago Pinto Dias
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gtk, Pango } = imports.gi;
const { Connection } = imports.connection;
const { Contact } = imports.contact;
//var store;

var CrudjsWindow = GObject.registerClass({
    GTypeName: 'CrudjsWindow',
    Template: 'resource:///org/gnome/crudjs/window.ui',
    InternalChildren: [
      'stackButton',
      'stack',
      'treeView',
      'entryFirstName',
      'entryLastName',
      'entryContact',
      'entryId',
      'buttonSave',
      'boxRegister',
      'boxList',
      'entrySearch',
      'buttonDelete'
    ]
}, class CrudjsWindow extends Gtk.ApplicationWindow {
    _init(application) {
        super._init({ application });

        this._search_text = null;

        let store = this._get_contacts();

        let normal = new Gtk.CellRendererText ();

        let id = new Gtk.TreeViewColumn ({ title: "Id" });
        let firstName = new Gtk.TreeViewColumn ({ title: "Nome" });
        let lastName = new Gtk.TreeViewColumn ({ title: "Sobrenome" });
        let contact = new Gtk.TreeViewColumn ({ title: "Contato" });

        id.pack_start(normal, true);
        firstName.pack_start(normal, true);
        lastName.pack_start(normal, true);
        contact.pack_start(normal, true);

        id.add_attribute (normal, "text", 0);
        firstName.add_attribute (normal, "text", 1);
        lastName.add_attribute (normal, "text", 2);
        contact.add_attribute (normal, "text", 3);

        id.set_sort_column_id(0);
        firstName.set_sort_column_id(1);
        lastName.set_sort_column_id(2);
        contact.set_sort_column_id(3);

        this._treeView.insert_column(id, 0);
        this._treeView.insert_column(firstName, 1);
        this._treeView.insert_column(lastName, 2);
        this._treeView.insert_column(contact, 3);
        this._treeView.set_model(store);
        //store = this._store;

        this._select = this._treeView.get_selection();
        this._select_handler_id = this._select.connect("changed", this._on_tree_selection_changed.bind(this));

        this._buttonSave.connect("clicked", this._on_clicked_button_save.bind(this));
        this._buttonDelete.connect("clicked", this._on_clicked_button_delete.bind(this));
        this._stackButton.connect("button-release-event", this._clear_entry.bind(this));
        this._entrySearch.connect("changed", this._on_changed_entrySearch.bind(this));

        this.show_all();
    }

    _on_tree_selection_changed(selection){

      let [ isSelected, model, iter ] = selection.get_selected();
      this._entryId.set_text(model.get_value(iter, 0));
      this._entryFirstName.set_text(model.get_value(iter, 1));
      this._entryLastName.set_text(model.get_value(iter, 2));
      this._entryContact.set_text(model.get_value(iter, 3));
      this._buttonDelete.set_visible(true);
      this._stack.set_visible_child(this._boxRegister);
    }

    _on_clicked_button_save(){

      let contact = new Contact();
      if(this._entryId.get_text() != ""){
        contact._id = this._entryId.get_text();
      }
      contact.set_firstname(this._entryFirstName.get_text());
      contact.set_lastname(this._entryLastName.get_text());
      contact.set_contact(this._entryContact.get_text());

      if(this._entryFirstName.get_text() != "" && this._entryContact.get_text() != ""){
        contact.save();
        this._select.disconnect(this._select_handler_id);
        let store = this._get_contacts();
        this._treeView.set_model(store);
        this._select_handler_id = this._select.connect("changed", this._on_tree_selection_changed.bind(this));
        this._buttonDelete.set_visible(false);
        this._stack.set_visible_child(this._boxList);
      }else{
        log("Olha, o campo nome e contato são obrigatorios!")
      }

    }

    _get_contacts(){

      let store = new Gtk.ListStore();
      store.set_column_types ([
            GObject.TYPE_STRING,
            GObject.TYPE_STRING,
            GObject.TYPE_STRING,
            GObject.TYPE_STRING]);

      let contacs = Contact.get_all();

        while (contacs.move_next ()) {
          store.set (store.append(), [0, 1, 2, 3],
            [
              contacs.get_value_at(0),
              contacs.get_value_at(1),
              contacs.get_value_at(2),
              contacs.get_value_at(3),
            ]);
          }

      let search_filter = store.filter_new(null);
      search_filter.set_visible_func(this._search_filter_func.bind(this));

      return search_filter;

    }

    _clear_entry(){

      this._buttonDelete.set_visible(false);
      this._entryId.set_text("");
      this._entryFirstName.set_text("");
      this._entryLastName.set_text("");
      this._entryContact.set_text("");

    }

    _search_filter_func(model, iter){

      if(this._search_text == null || this._search_text == ""){
        return true;
      }else{
        let firstName = model.get_value(iter, 1).toString();
        let lastName = model.get_value(iter, 2).toString();
        firstName = firstName.toLowerCase();
        lastName = lastName.toLowerCase();
        let search_text = this._search_text.toString().toLowerCase();


        if(firstName.includes(search_text) || lastName.includes(search_text)){
          return true;
        }else{
          return false;
        }

      }

    }

    _on_changed_entrySearch(){
      this._search_text = this._entrySearch.get_text();
      log(this._search_text);
      let store = this._get_contacts();
      this._treeView.set_model(store);
      this._search_text = null;
    }

    _on_clicked_button_delete(){

      let contact = new Contact();
      if(this._entryId.get_text() != ""){
        contact._id = this._entryId.get_text();
      }
      contact.set_firstname(this._entryFirstName.get_text());
      contact.set_lastname(this._entryLastName.get_text());
      contact.set_contact(this._entryContact.get_text());

      contact.delete();
      this._select.disconnect(this._select_handler_id);
      let store = this._get_contacts();
      this._treeView.set_model(store);
      this._select_handler_id = this._select.connect("changed", this._on_tree_selection_changed.bind(this));
      this._buttonDelete.set_visible(false);
      this._stack.set_visible_child(this._boxList);
    }

});

